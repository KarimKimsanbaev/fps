using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthView : MonoBehaviour
{
    private Text _text;
    private PlayerHealth _playerHealth;

    void Start()
    {
        _text = GetComponent<Text>();
        _playerHealth = GameObject.Find("Character").GetComponent<PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        _text.text = _playerHealth.Health.ToString();
    }
}
